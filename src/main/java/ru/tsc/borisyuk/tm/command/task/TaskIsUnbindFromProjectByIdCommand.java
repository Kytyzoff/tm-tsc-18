package ru.tsc.borisyuk.tm.command.task;

import ru.tsc.borisyuk.tm.command.AbstractTaskCommand;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.exception.entity.TaskNotFoundException;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public class TaskIsUnbindFromProjectByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-unbind-from-project-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unbind task from project by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(projectId) == null) throw new ProjectNotFoundException();
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (serviceLocator.getTaskService().findById(taskId) == null) throw new TaskNotFoundException();
        serviceLocator.getProjectTaskService().unbindTaskById(projectId, taskId);
    }

}
