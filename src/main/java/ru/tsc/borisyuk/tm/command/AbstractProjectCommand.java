package ru.tsc.borisyuk.tm.command;

import ru.tsc.borisyuk.tm.exception.empty.EmptyNameException;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.model.Project;

import java.util.Date;
import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(Project project) {
        final List<Project> projects = serviceLocator.getProjectService().findAll();
        final Integer indexNum = projects.indexOf(project) + 1;
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Index: " + indexNum);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
    }

    protected Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return new Project(name, description, new Date());
    }

}
