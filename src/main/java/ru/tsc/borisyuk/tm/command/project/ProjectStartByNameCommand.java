package ru.tsc.borisyuk.tm.command.project;

import ru.tsc.borisyuk.tm.command.AbstractProjectCommand;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start project by name...";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

}
