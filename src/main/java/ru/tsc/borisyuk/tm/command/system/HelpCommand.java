package ru.tsc.borisyuk.tm.command.system;

import ru.tsc.borisyuk.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display all commands with description...";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");

        /*final Command[] commands = serviceLocator.getCommandService().getTerminalCommands();
        for (final Command command : commands) System.out.println(command);*/

        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands())
            System.out.println(command.name() + ": " + command.description());
    }

}
