package ru.tsc.borisyuk.tm.api.entity;

public interface IWBS extends IHasCreated, IHasName, IHasStartDate, IHasStatus {
}
