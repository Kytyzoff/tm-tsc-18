package ru.tsc.borisyuk.tm.api.service;

import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByEmail(String email);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User removeUser(User user);

    User findByLogin(String login);

    User removeById(String id);

    User removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(String userId, String firstName, String lastName, String middleName);

}
