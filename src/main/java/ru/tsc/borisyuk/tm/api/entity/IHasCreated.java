package ru.tsc.borisyuk.tm.api.entity;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

    void setCreated(Date created);

}
