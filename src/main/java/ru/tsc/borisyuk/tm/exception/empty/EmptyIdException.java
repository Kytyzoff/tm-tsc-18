package ru.tsc.borisyuk.tm.exception.empty;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error. Id cannot take in an empty String or null value.");
    }

    public EmptyIdException(String value) {
        super("Error. Id cannot take in " + value + ".");
    }

}
