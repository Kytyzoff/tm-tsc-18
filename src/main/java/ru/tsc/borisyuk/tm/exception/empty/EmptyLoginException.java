package ru.tsc.borisyuk.tm.exception.empty;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error. Login is empty.");
    }

    public EmptyLoginException(String value) {
        super("Error." + value + " Login is empty.");
    }

}
